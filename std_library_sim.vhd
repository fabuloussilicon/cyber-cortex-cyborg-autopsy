-- *****************************************************************************************
-- 
-- Version 1.1
-- Modified 05.01.2007
-- Designed by Ruslan Lepetenok
-- Reverse function is added
-- *****************************************************************************************

library	IEEE;
use IEEE.std_logic_1164.all;

library	STD;
use STD.textio.all;

use WORK.std_library.all;

package std_library_sim is

subtype	nibble_stype is std_logic_vector(3 downto 0);
	
-- Functions declarations
function fn_vect_asc_rev (vect : std_logic_vector) return std_logic_vector;
function fn_to_hex_string(vect : std_logic_vector) return string;
function fn_to_int       (char : character) return integer;
function fn_to_char      (nibble   : nibble_stype) return character;
function fn_to_char      (nibble   : integer range 0 to 15) return character;
function fn_res_str_len  (int   : integer) return integer;
function fn_to_hex_string(int   : integer) return string;
function fn_log2_sim     (arg : integer) return integer;

-- Procedures declarations
procedure write_ms(L : inout line ; str : string);

end std_library_sim;

package	body std_library_sim is

-- Functions		
function fn_vect_asc_rev(vect : std_logic_vector) return std_logic_vector is	
variable result : std_logic_vector(vect'high downto vect'low);
begin
 for i in result'range loop
  result(i) := vect(vect'high-i);	 
 end loop;	 
return result;	
end fn_vect_asc_rev;


function fn_to_hex_string(vect : std_logic_vector) return string is
variable res_str_a : string (1 to vect'length/4) := (others => '0'); -- TBD
variable res_str_b : string (1 to vect'length/4+1) := (others => '0'); -- TBD
variable temp_vect : std_logic_vector(vect'high+(vect'length-(vect'length/4)*4) downto 0) := (others => '0');

begin
	
 if vect'ascending then
  temp_vect(vect'high-vect'low downto 0) := fn_vect_asc_rev(vect);	
 else
  temp_vect(vect'high-vect'low downto 0) := vect;	
 end if;
	
 if((vect'length/4)*4=vect'length) then 	
  for i in 1 to vect'length/4 loop	
   res_str_a(vect'length/4-i+1) := fn_to_char(fn_to_integer(temp_vect((i-1)*4+3 downto (i-1)*4)));
  end loop;	
  return res_str_a;	
 else 
  for i in 1 to vect'length/4+1 loop
   exit when (i-1)*4+3 > temp_vect'high;	 
   res_str_b(vect'length/4+1-i+1) := fn_to_char(fn_to_integer(temp_vect((i-1)*4+3 downto (i-1)*4)));
  end loop;	
  return res_str_b;	
end if;

end fn_to_hex_string;

-- Converts symbols 0..9,A..F(a..f) to natural
function fn_to_int(char : character) return integer is 
variable result : integer := 0;
begin
if (char>='0' and char<='9') then
 result := character'pos(char)-character'pos('0');
  elsif (char>='A' and char<='F') then
   result := character'pos(char)-character'pos('A')+10;
   elsif (char>='a' and char<='f') then	
    result := character'pos(char)-character'pos('a')+10;
    else report"Error in hexadecimal" severity FAILURE;	   
	end if;	
return (result);
end fn_to_int;

function fn_to_char(nibble : nibble_stype) return character is
variable result : character;
begin
 case(nibble)is
  when "0000" => result := '0';	
  when "0001" => result := '1';	
  when "0010" => result := '2';	 
  when "0011" => result := '3';	  
  when "0100" => result := '4';	
  when "0101" => result := '5';	
  when "0110" => result := '6';	 
  when "0111" => result := '7';	
  when "1000" => result := '8';	
  when "1001" => result := '9';	
  when "1010" => result := 'A';	 
  when "1011" => result := 'B';	  
  when "1100" => result := 'C';	
  when "1101" => result := 'D';	
  when "1110" => result := 'E';	 
  when "1111" => result := 'F';
  when others => result := '0';	
 end case;
return (result);
end fn_to_char;

function fn_to_char(nibble : integer range 0 to 15) return character is
variable result : character;
begin

--if nibble <= 9 then -- 0..9
-- result := character'val(character'pos('0') + nibble);
--else			        -- A..F
-- result := character'val(character'pos('A') + nibble - 10);
--end if;		
	
 case(nibble)is
  when 0 => result := '0';	
  when 1 => result := '1';	
  when 2 => result := '2';	 
  when 3 => result := '3';	  
  when 4 => result := '4';	
  when 5 => result := '5';	
  when 6 => result := '6';	 
  when 7 => result := '7';	
  when 8 => result := '8';	
  when 9 => result := '9';	
  when 10 => result := 'A';	 
  when 11 => result := 'B';	  
  when 12 => result := 'C';	
  when 13 => result := 'D';	
  when 14 => result := 'E';	 
  when 15 => result := 'F';
  when others => result := '0';	
 end case;
return (result);
end fn_to_char;


function fn_res_str_len(int   : integer) return integer is 
begin
 for i in 1 to 8 loop
  if(int<2**(4*i)) then 
   return (i);
  end if;
 end loop;	 
return (0);
end fn_res_str_len;


function fn_to_hex_string(int   : integer) return string is 
begin
return (fn_to_hex_string(fn_to_std_logic_vector(int,32)));
end fn_to_hex_string;

function fn_log2_sim(arg : integer) return integer is
variable temp : integer;
begin
 temp := 1;
 if (arg=1) then 
  return 0;
 else 
  for i in 1 to integer'high loop
   temp := 2*temp; 
   if (temp>=arg) then 
    return i;
   end if;
  end loop;
 end if;	
end fn_log2_sim;	

-- Procedures
procedure write_ms(L : inout line ; str : string) is
variable i : integer;
variable tmp_str : string(str'range);
begin
 i := 1;
 while i <= str'length loop
  tmp_str(i) := str(i); 
  i := i+1;
 end loop;
 write(L,tmp_str);
end write_ms;

end std_library_sim;	
	
	
