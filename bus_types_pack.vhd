-- *****************************************************************************************
-- 
-- Version 0.2
-- Modified 31.01.2007
-- Designed by Ruslan Lepetenok
-- *****************************************************************************************

library	IEEE;
use IEEE.std_logic_1164.all;

package bus_types_pack is

-- I/O slaves
type io_slv_out_single_type is record
 dbusout : std_logic_vector(7 downto 0); 
 out_en  : std_logic;	
end record;	

type io_slv_out_type is array (natural range <>) of io_slv_out_single_type;

-- DM slaves
type mem_slv_out_single_type is record
 dbusout : std_logic_vector(7 downto 0); 
 out_en  : std_logic;	
 cpuwait : std_logic; -- Polarity ???
end record;	

type mem_slv_out_type is array (natural range <>) of mem_slv_out_single_type;

-- DM masters
type mem_mst_out_single_type is record
 ramadr  : std_logic_vector(15 downto 0); 
 dbusout : std_logic_vector(7 downto 0);
 ramre   : std_logic;
 ramwe   : std_logic;
end record; 

type mem_mst_out_type is array (natural range <>) of mem_mst_out_single_type;

type mem_mst_in_single_type is record
 dbusin   : std_logic_vector(7 downto 0);
 mst_wait : std_logic;
end record; 

type mem_mst_in_type is array (natural range <>) of mem_mst_in_single_type;

--***************** External RAM support *************************

type ext_ram_adr_single_type is record
 adr_h : std_logic_vector(7 downto 0);
 len   : std_logic_vector(7 downto 0);
end record;	

type ext_ram_adr_type is array (natural range <>) of ext_ram_adr_single_type;

end bus_types_pack;