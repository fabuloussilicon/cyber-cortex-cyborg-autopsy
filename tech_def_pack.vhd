-- *****************************************************************************************
-- 
-- Version 0.2
-- Modified 17.06.2007
-- Designed by Ruslan Lepetenok
-- *****************************************************************************************

library	IEEE;
use IEEE.std_logic_1164.all;

package tech_def_pack is

constant c_tech_generic     : integer := 0;
-- Xilinx
constant c_tech_virtex      : integer := 1;
constant c_tech_virtex_e    : integer := 2;
constant c_tech_virtex_ii   : integer := 3;
constant c_tech_virtex_4    : integer := 4;
constant c_tech_virtex_5    : integer := 5; 
constant c_tech_spartan_3   : integer := 6;
-- Altera
constant c_acex             : integer := 7;

end tech_def_pack;	
	
	
