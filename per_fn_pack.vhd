-- *****************************************************************************************
-- Peripheral functions
-- Version 0.1
-- Modified 04.12.2006
-- Designed by Ruslan Lepetenok
-- *****************************************************************************************

library	IEEE;
use IEEE.std_logic_1164.all;

use WORK.std_library.all;

package per_fn_pack is

type port_rd_single_type is record	
 port_current : std_logic_vector(7 downto 0);
 port_adr     : integer;
 use_dm       : integer;
 impl_port    : integer;
end record;	



type ports_rd_type is array (natural range <>) of port_rd_single_type;

function fn_rd_io_port(
                       ports   : ports_rd_type;
                       adr     : std_logic_vector 
					   ) return std_logic_vector;
					   
function fn_rd_dm_port(
                       ports   : ports_rd_type;
                       ramadr  : std_logic_vector
					   ) return std_logic_vector;

					   
function fn_gen_io_out_en(
                          ports   : ports_rd_type;
                          adr     : std_logic_vector; 
                          iore    : std_logic
						  ) return std_logic;
					   
function fn_gen_dm_out_en(
                          ports   : ports_rd_type;
                          ramadr  : std_logic_vector;
                          ramre   : std_logic; 
                          dm_sel  : std_logic  
						  ) return std_logic;
			  
						  
function fn_wr_port(
                    port_current : std_logic_vector;
                    port_adr     : integer;
                    use_dm       : integer;  
					adr          : std_logic_vector; 
					iowe         : std_logic;
					dbus_in      : std_logic_vector;
					ramadr       : std_logic_vector;
					dm_sel		 : std_logic;
					ramwe        : std_logic; 
					dm_dbus_in   : std_logic_vector
                    ) return std_logic_vector;


function fn_wr_port(
                    port_current : std_logic_vector;
                    port_adr     : integer;
                    use_dm       : integer;  
					adr          : std_logic_vector; 
					iowe         : std_logic;
					dbus_in      : std_logic_vector;
					ramadr       : std_logic_vector;
					dm_sel		 : std_logic;
					ramwe        : std_logic; 
					dm_dbus_in   : std_logic_vector;
					mask         : std_logic_vector(7 downto 0);
					init_val     : std_logic_vector(7 downto 0)
                    ) return std_logic_vector;					
					
					
function fn_exp_to_byte(in_vect : std_logic_vector) return std_logic_vector;

function fn_wr_port_mux(
                        use_dm       : integer;  
					    dbus_in      : std_logic_vector;
					    dm_dbus_in   : std_logic_vector
                        ) return std_logic_vector;
						
function fn_wr_port_en(
                    port_adr     : integer;
                    use_dm       : integer;  
					adr          : std_logic_vector; 
					iowe         : std_logic;
					ramadr       : std_logic_vector;
					dm_sel		 : std_logic;
					ramwe        : std_logic 
                    ) return std_logic;      
					
end per_fn_pack;  

package body per_fn_pack is

function fn_wr_port(
                    port_current : std_logic_vector;
                    port_adr     : integer;
                    use_dm       : integer;  
					adr          : std_logic_vector; 
					iowe         : std_logic;
					dbus_in      : std_logic_vector;
					ramadr       : std_logic_vector;
					dm_sel		 : std_logic;
					ramwe        : std_logic; 
					dm_dbus_in   : std_logic_vector
                    ) return std_logic_vector is      
					
variable port_next : std_logic_vector(port_current'length-1 downto 0);
begin
 port_next := port_current; 
 
 case(use_dm) is
  when 0	  => -- I/O
   if(fn_to_integer(adr)=port_adr and iowe='1') then
  	port_next := dbus_in(port_next'range); 
   end if;	   
  when others => -- DM
   if(fn_to_integer(ramadr)=port_adr and dm_sel='1' and ramwe='1') then
  	port_next := dm_dbus_in(port_next'range); 
   end if;	   
 end case;
 return port_next; 
end fn_wr_port;


function fn_wr_port_mux(
                        use_dm       : integer;  
					    dbus_in      : std_logic_vector;
					    dm_dbus_in   : std_logic_vector
                        ) return std_logic_vector is      
					
variable port_next : std_logic_vector(7 downto 0);
begin
 port_next := (others => '0'); 
 case(use_dm) is
  when 0	  => -- I/O
   port_next := dbus_in(port_next'range); 
  when others => -- DM
   port_next := dm_dbus_in(port_next'range); 
 end case; 
 return port_next;
end fn_wr_port_mux;


function fn_wr_port_en(
                    port_adr     : integer;
                    use_dm       : integer;  
					adr          : std_logic_vector; 
					iowe         : std_logic;
					ramadr       : std_logic_vector;
					dm_sel		 : std_logic;
					ramwe        : std_logic 
                    ) return std_logic is      
					
variable tmp : std_logic;
begin
 tmp := '0'; 
 
 case(use_dm) is
  when 0	  => -- I/O
   if(fn_to_integer(adr)=port_adr and iowe='1') then
  	tmp := '1'; 
   end if;	   
  when others => -- DM
   if(fn_to_integer(ramadr)=port_adr and dm_sel='1' and ramwe='1') then
  	tmp := '1'; 
   end if;	   
 end case; 
 return tmp;
end fn_wr_port_en;


function fn_rd_io_port(
                       ports   : ports_rd_type;
                       adr     : std_logic_vector 
					   ) return std_logic_vector is
					   
variable result : std_logic_vector(7 downto 0);
begin
 result := (others => '0');
 for i in ports'range loop
  if(ports(i).use_dm=0 and fn_to_integer(adr)=ports(i).port_adr) then
   result := ports(i).port_current;
   exit;
  end if; 
 end loop;	 
 return result;
end fn_rd_io_port;

function fn_rd_dm_port(
                       ports   : ports_rd_type;
                       ramadr  : std_logic_vector
					   ) return std_logic_vector is
					   
variable result : std_logic_vector(7 downto 0);
begin
 result := (others => '0');
 for i in ports'range loop
  if(ports(i).use_dm/=0 and fn_to_integer(ramadr)=ports(i).port_adr) then
   result := ports(i).port_current;
   exit;
  end if; 
 end loop;	 
 return result;
end fn_rd_dm_port;

function fn_gen_io_out_en(
                          ports   : ports_rd_type;
                          adr     : std_logic_vector; 
                          iore    : std_logic
						  ) return std_logic is
variable result : std_logic;
begin
 result := '0';
 for i in ports'range loop
  if(ports(i).use_dm=0 and ports(i).impl_port/=0 and fn_to_integer(adr)=ports(i).port_adr) then
   result := iore;
   exit;
  end if; 
 end loop;	 
 return result;
end fn_gen_io_out_en;

function fn_gen_dm_out_en(
                          ports   : ports_rd_type;
                          ramadr  : std_logic_vector;
                          ramre   : std_logic; 
                          dm_sel  : std_logic  
						  ) return std_logic is
variable result : std_logic;
begin
 result := '0';
 for i in ports'range loop
  if(ports(i).use_dm/=0 and ports(i).impl_port/=0 and fn_to_integer(ramadr)=ports(i).port_adr) then
   result := ramre and dm_sel;
   exit;
  end if; 
 end loop;	 
 return result;
end fn_gen_dm_out_en;


function fn_exp_to_byte(in_vect : std_logic_vector) return std_logic_vector is
variable result : std_logic_vector(7 downto 0);
begin
 result := (others => '0');
 result(in_vect'range) := in_vect;
 return result;
end fn_exp_to_byte;


function fn_wr_port(
                    port_current : std_logic_vector;
                    port_adr     : integer;
                    use_dm       : integer;  
					adr          : std_logic_vector; 
					iowe         : std_logic;
					dbus_in      : std_logic_vector;
					ramadr       : std_logic_vector;
					dm_sel		 : std_logic;
					ramwe        : std_logic; 
					dm_dbus_in   : std_logic_vector;
					mask         : std_logic_vector(7 downto 0);
					init_val     : std_logic_vector(7 downto 0)
                    ) return std_logic_vector is					
					
variable port_next : std_logic_vector(port_current'length-1 downto 0);
begin
 port_next := port_current; 
 
 case(use_dm) is
  when 0	  => -- I/O
   if(fn_to_integer(adr)=port_adr and iowe='1') then
  	port_next := (dbus_in(port_next'range) and mask(port_next'range)) or
	             (init_val(port_next'range) and not mask(port_next'range)); 
   end if;	   
  when others => -- DM
   if(fn_to_integer(ramadr)=port_adr and dm_sel='1' and ramwe='1') then
	port_next := (dm_dbus_in(port_next'range) and mask(port_next'range)) or
	             (init_val(port_next'range) and not mask(port_next'range));   
   end if;	   
 end case;
 return port_next; 
end fn_wr_port;
				
					
end per_fn_pack;	
