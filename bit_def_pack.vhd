-- *****************************************************************************************
-- 
-- Version 0.1
-- Modified 03.01.2007
-- Designed by Ruslan Lepetenok
-- *****************************************************************************************

library	IEEE;
use IEEE.std_logic_1164.all;

package bit_def_pack is

-- Bit definitions for use with the IAR Assembler   
-- The Register Bit names are represented by their bit number (0-7).

-- USART1 Control and Status Register C 
constant    UMSEL1_bit : integer := 6;
constant    UPM11_bit  : integer := 5;
constant    UPM10_bit  : integer := 4;
constant    USBS1_bit  : integer := 3;
constant    UCSZ11_bit : integer := 2;
constant    UCSZ10_bit : integer := 1;
constant    UCPOL1_bit : integer := 0;

-- USART1 Control and Status Register A 
constant    RXC1_bit  : integer := 7;
constant    TXC1_bit  : integer := 6;
constant    UDRE1_bit : integer := 5;
constant    FE1_bit   : integer := 4;
constant    DOR1_bit  : integer := 3;
constant    UPE1_bit  : integer := 2;
constant    U2X1_bit  : integer := 1;
constant    MPCM1_bit : integer := 0;

-- USART1 Control and Status Register B 
constant    RXCIE1_bit : integer := 7;
constant    TXCIE1_bit : integer := 6;
constant    UDRIE1_bit : integer := 5;
constant    RXEN1_bit  : integer := 4;
constant    TXEN1_bit  : integer := 3;
constant    UCSZ12_bit : integer := 2;
constant    RXB81_bit  : integer := 1;
constant    TXB81_bit  : integer := 0;
								 
-- USART0 Control and Status Register C 
constant    UMSEL0_bit : integer := 6;
constant    UPM01_bit  : integer := 5;
constant    UPM00_bit  : integer := 4;
constant    USBS0_bit  : integer := 3;
constant    UCSZ01_bit : integer := 2;
constant    UCSZ00_bit : integer := 1;
constant    UCPOL0_bit : integer := 0;

-- USART0 Control and Status Register A 
constant    RXC0_bit   : integer := 7;
constant    TXC0_bit   : integer := 6;
constant    UDRE0_bit  : integer := 5;
constant    FE0_bit    : integer := 4;
constant    DOR0_bit   : integer := 3;
constant    UPE0_bit   : integer := 2;
constant    U2X0_bit   : integer := 1;
constant    MPCM0_bit  : integer := 0;

-- USART0 Control and Status Register B 
constant    RXCIE0_bit : integer := 7;
constant    TXCIE0_bit : integer := 6;
constant    UDRIE0_bit : integer := 5;
constant    RXEN0_bit  : integer := 4;
constant    TXEN0_bit  : integer := 3;
constant    UCSZ02_bit : integer := 2;
constant    RXB80_bit  : integer := 1;
constant    TXB80_bit  : integer := 0;

-- Timer/Counter 3 Control Register C 
constant    FOC3A_bit  : integer := 7;
constant    FOC3B_bit  : integer := 6;
constant    FOC3C_bit  : integer := 5;

-- Timer/Counter 3 Control Register A 
constant    COM3A1_bit : integer := 7;
constant    COM3A0_bit : integer := 6;
constant    COM3B1_bit : integer := 5;
constant    COM3B0_bit : integer := 4;
constant    COM3C1_bit : integer := 3;
constant    COM3C0_bit : integer := 2;
constant    WGM31_bit  : integer := 1;
constant    WGM30_bit  : integer := 0;

-- Timer/Counter 3 Control Register B 
constant    ICNC3_bit : integer := 7;
constant    ICES3_bit : integer := 6;
constant    WGM33_bit : integer := 4;
constant    WGM32_bit : integer := 3;
constant    CS32_bit  : integer := 2;
constant    CS31_bit  : integer := 1;
constant    CS30_bit  : integer := 0;

-- Extended Timer/Counter Interrupt Mask Register 
constant    TICIE3_bit : integer := 5;
constant    OCIE3A_bit : integer := 4;
constant    OCIE3B_bit : integer := 3;
constant    TOIE3_bit  : integer := 2;
constant    OCIE3C_bit : integer := 1;
constant    OCIE1C_bit : integer := 0;

-- Extended Timer/Counter Interrupt Flag Register 
constant    ICF3_bit  : integer := 5;
constant    OCF3A_bit : integer := 4;
constant    OCF3B_bit : integer := 3;
constant    TOV3_bit  : integer := 2;
constant    OCF3C_bit : integer := 1;
constant    OCF1C_bit : integer := 0;

-- TWI Control Register 
constant    TWINT_bit : integer := 7;
constant    TWEA_bit  : integer := 6;
constant    TWSTA_bit : integer := 5;
constant    TWSTO_bit : integer := 4;
constant    TWWC_bit  : integer := 3;
constant    TWEN_bit  : integer := 2;
constant    TWIE_bit  : integer := 0;

-- TWI (slave) Address Register 
constant    TWA6_bit  : integer := 7;
constant    TWA5_bit  : integer := 6;
constant    TWA4_bit  : integer := 5;
constant    TWA3_bit  : integer := 4;
constant    TWA2_bit  : integer := 3;
constant    TWA1_bit  : integer := 2;
constant    TWA0_bit  : integer := 1;
constant    TWGCE_bit : integer := 0;

-- TWI Status Register 
constant    TWS7_bit  : integer := 7;
constant    TWS6_bit  : integer := 6;
constant    TWS5_bit  : integer := 5;
constant    TWS4_bit  : integer := 4;
constant    TWS3_bit  : integer := 3;
constant    TWPS1_bit : integer := 1;
constant    TWPS0_bit : integer := 0;

-- External Memory Control Register A 
constant    SRL2_bit  : integer := 6;
constant    SRL1_bit  : integer := 5;
constant    SRL0_bit  : integer := 4;
constant    SRW01_bit : integer := 3;
constant    SRW00_bit : integer := 2;
constant    SRW11_bit : integer := 1;

-- External Memory Control Register B 
constant    XMBK_bit  : integer := 7; 
constant    XMM2_bit  : integer := 2;
constant    XMM1_bit  : integer := 1;
constant    XMM0_bit  : integer := 0;

-- External Interrupt Control Register A 
constant    ISC31_bit : integer := 7;
constant    ISC30_bit : integer := 6;
constant    ISC21_bit : integer := 5;
constant    ISC20_bit : integer := 4;
constant    ISC11_bit : integer := 3;
constant    ISC10_bit : integer := 2;
constant    ISC01_bit : integer := 1;
constant    ISC00_bit : integer := 0;

-- Store Program Memory Control and Status Register 
constant    SPMIE_bit  : integer := 7;
constant    RWWSB_bit  : integer := 6;
constant    RWWSRE_bit : integer := 4;
constant    BLBSET_bit : integer := 3;
constant    PGWRT_bit  : integer := 2;
constant    PGERS_bit  : integer := 1;
constant    SPMEN_bit  : integer := 0;

-- Data Register, Port G 
constant    PG4_bit : integer := 4;
constant    PG3_bit : integer := 3;
constant    PG2_bit : integer := 2;
constant    PG1_bit : integer := 1;
constant    PG0_bit : integer := 0;

 -- Data Register, Port G 
constant    PORTG4_bit : integer :=  4;
constant    PORTG3_bit : integer :=  3;
constant    PORTG2_bit : integer :=  2;
constant    PORTG1_bit : integer :=  1;
constant    PORTG0_bit : integer :=  0;

-- Data Direction Register, Port G 
constant    DDG4_bit : integer := 4;
constant    DDG3_bit : integer := 3;
constant    DDG2_bit : integer := 2;
constant    DDG1_bit : integer := 1;
constant    DDG0_bit : integer := 0;

-- Input Pins, Port G 
constant    PING4_bit : integer := 4;
constant    PING3_bit : integer := 3;
constant    PING2_bit : integer := 2;
constant    PING1_bit : integer := 1;
constant    PING0_bit : integer := 0;

-- Data Register, Port F 
constant    PF7_bit : integer := 7;
constant    PF6_bit : integer := 6;
constant    PF5_bit : integer := 5;
constant    PF4_bit : integer := 4;
constant    PF3_bit : integer := 3;
constant    PF2_bit : integer := 2;
constant    PF1_bit : integer := 1;
constant    PF0_bit : integer := 0;
 
-- Data Register, Port F 
constant    PORTF7_bit : integer := 7;
constant    PORTF6_bit : integer := 6;
constant    PORTF5_bit : integer := 5;
constant    PORTF4_bit : integer := 4;
constant    PORTF3_bit : integer := 3;
constant    PORTF2_bit : integer := 2;
constant    PORTF1_bit : integer := 1;
constant    PORTF0_bit : integer := 0;
 
-- Data Direction Register, Port F 
constant    DDF7_bit : integer := 7;
constant    DDF6_bit : integer := 6;
constant    DDF5_bit : integer := 5;
constant    DDF4_bit : integer := 4;
constant    DDF3_bit : integer := 3;
constant    DDF2_bit : integer := 2;
constant    DDF1_bit : integer := 1;
constant    DDF0_bit : integer := 0;
 
-- Input Pins, Port F 
constant    PINF7_bit : integer := 7;
constant    PINF6_bit : integer := 6;
constant    PINF5_bit : integer := 5;
constant    PINF4_bit : integer := 4;
constant    PINF3_bit : integer := 3;
constant    PINF2_bit : integer := 2;
constant    PINF1_bit : integer := 1;
constant    PINF0_bit : integer := 0;

-- Stack Pointer High 
constant    SP15_bit : integer := 7;
constant    SP14_bit : integer := 6;
constant    SP13_bit : integer := 5;
constant    SP12_bit : integer := 4;
constant    SP11_bit : integer := 3;
constant    SP10_bit : integer := 2;
constant    SP9_bit : integer := 1;
constant    SP8_bit : integer := 0;

-- Stack Pointer Low 
constant    SP7_bit : integer := 7;
constant    SP6_bit : integer := 6;
constant    SP5_bit : integer := 5;
constant    SP4_bit : integer := 4;
constant    SP3_bit : integer := 3;
constant    SP2_bit : integer := 2;
constant    SP1_bit : integer := 1;
constant    SP0_bit : integer := 0;

-- XTAL Divide Control Register 
constant    XDIVEN_bit : integer := 7;
constant    XDIV6_bit  : integer := 6;
constant    XDIV5_bit  : integer := 5;
constant    XDIV4_bit  : integer := 4;
constant    XDIV3_bit  : integer := 3;
constant    XDIV2_bit  : integer := 2;
constant    XDIV1_bit  : integer := 1;
constant    XDIV0_bit  : integer := 0;

-- RAM Page Z Select Register 
constant    RAMPZ0_bit : integer := 0;

-- External Interrupt Control Register B 
constant    ISC71_bit : integer := 7;
constant    ISC70_bit : integer := 6;
constant    ISC61_bit : integer := 5;
constant    ISC60_bit : integer := 4;
constant    ISC51_bit : integer := 3;
constant    ISC50_bit : integer := 2;
constant    ISC41_bit : integer := 1;
constant    ISC40_bit : integer := 0;

-- External Interrupt Mask Register 
constant    INT7_bit : integer := 7;
constant    INT6_bit : integer := 6;
constant    INT5_bit : integer := 5;
constant    INT4_bit : integer := 4;
constant    INT3_bit : integer := 3;
constant    INT2_bit : integer := 2;
constant    INT1_bit : integer := 1;
constant    INT0_bit : integer := 0;

-- External Interrupt Flag Register 
constant    INTF7_bit : integer := 7;
constant    INTF6_bit : integer := 6;
constant    INTF5_bit : integer := 5;
constant    INTF4_bit : integer := 4;
constant    INTF3_bit : integer := 3;
constant    INTF2_bit : integer := 2;
constant    INTF1_bit : integer := 1;
constant    INTF0_bit : integer := 0;

-- Timer/Counter Interrupt Mask Register 
constant    OCIE2_bit  : integer := 7;
constant    TOIE2_bit  : integer := 6;
constant    TICIE1_bit : integer := 5;
constant    OCIE1A_bit : integer := 4;
constant    OCIE1B_bit : integer := 3;
constant    TOIE1_bit  : integer := 2;
constant    OCIE0_bit  : integer := 1;
constant    TOIE0_bit  : integer := 0;

-- Timer/Counter Interrupt Flag Register 
constant    OCF2_bit  : integer := 7;
constant    TOV2_bit  : integer := 6;
constant    ICF1_bit  : integer := 5;
constant    OCF1A_bit : integer := 4;
constant    OCF1B_bit : integer := 3;
constant    TOV1_bit  : integer := 2;
constant    OCF0_bit  : integer := 1;
constant    TOV0_bit  : integer := 0;


-- MCU general Control Register 
constant    SRE_bit   : integer := 7;
constant    SRW10_bit : integer := 6;
constant    SE_bit    : integer := 5;
constant    SM1_bit   : integer := 4;
constant    SM0_bit   : integer := 3;
constant    SM2_bit   : integer := 2;
constant    IVSEL_bit : integer := 1;
constant    IVCE_bit  : integer := 0;

-- MCU general Control and Status Register 
constant    JTD_bit   : integer := 7;
constant    JTRF_bit  : integer := 4;
constant    WDRF_bit  : integer := 3;
constant    BORF_bit  : integer := 2;
constant    EXTRF_bit : integer := 1;
constant    PORF_bit  : integer := 0;

-- Timer/Counter 0 Control Register 
constant    FOC0_bit  : integer := 7;
constant    WGM00_bit : integer := 6;
constant    COM01_bit : integer := 5;
constant    COM00_bit : integer := 4;
constant    WGM01_bit : integer := 3;
constant    CS02_bit  : integer := 2;
constant    CS01_bit  : integer := 1;
constant    CS00_bit  : integer := 0;

-- Asynchronous mode Status Register 
constant    AS0_bit    : integer := 3;
constant    TCN0UB_bit : integer := 2;
constant    OCR0UB_bit : integer := 1;
constant    TCR0UB_bit : integer := 0;

-- Timer/Counter 1 Control Register C 
constant    FOC1A_bit : integer := 7;
constant    FOC1B_bit : integer := 6;
constant    FOC1C_bit : integer := 5;

-- Timer/Counter 1 Control Register A 
constant    COM1A1_bit : integer := 7;
constant    COM1A0_bit : integer := 6;
constant    COM1B1_bit : integer := 5;
constant    COM1B0_bit : integer := 4;
constant    COM1C1_bit : integer := 3;
constant    COM1C0_bit : integer := 2;
constant    WGM11_bit  : integer := 1;
constant    WGM10_bit  : integer := 0;

-- Timer/Counter 1 Control Register B 
constant    ICNC1_bit : integer := 7;
constant    ICES1_bit : integer := 6;
constant    WGM13_bit : integer := 4;
constant    WGM12_bit : integer := 3;
constant    CS12_bit  : integer := 2;
constant    CS11_bit  : integer := 1;
constant    CS10_bit  : integer := 0;

-- Timer/Counter 2 Control Register 
constant    FOC2_bit  : integer := 7;
constant    WGM20_bit : integer := 6;
constant    COM21_bit : integer := 5;
constant    COM20_bit : integer := 4;
constant    WGM21_bit : integer := 3;
constant    CS22_bit  : integer := 2;
constant    CS21_bit  : integer := 1;
constant    CS20_bit  : integer := 0;

-- On-Chip Debug Register 
constant    IDRD_bit  : integer := 7;
constant    OCDR7_bit : integer := 7;
constant    OCDR6_bit : integer := 6;
constant    OCDR5_bit : integer := 5;
constant    OCDR4_bit : integer := 4;
constant    OCDR3_bit : integer := 3;
constant    OCDR2_bit : integer := 2;
constant    OCDR1_bit : integer := 1;
constant    OCDR0_bit : integer := 0;

-- Watchdog Timer Control Register 
constant    WDCE_bit  : integer := 4;
constant    WDE_bit   : integer := 3;
constant    WDP2_bit  : integer := 2;
constant    WDP1_bit  : integer := 1;
constant    WDP0_bit  : integer := 0;

-- Special Function I/O Register 
constant    TSM_bit    : integer := 7;
constant    ADHSM_bit  : integer := 4;
constant    ACME_bit   : integer := 3;
constant    PUD_bit    : integer := 2;
constant    PSR0_bit   : integer := 1;
constant    PSR321_bit : integer := 0;

-- EEPROM Control Register 
constant    EERIE_bit : integer := 3;
constant    EEMWE_bit : integer := 2;
constant    EEWE_bit  : integer := 1;
constant    EERE_bit  : integer := 0;

-- Data Register, Port A 
constant    PA7_bit : integer := 7;
constant    PA6_bit : integer := 6;
constant    PA5_bit : integer := 5;
constant    PA4_bit : integer := 4;
constant    PA3_bit : integer := 3;
constant    PA2_bit : integer := 2;
constant    PA1_bit : integer := 1;
constant    PA0_bit : integer := 0;
 
-- Data Register, Port A 
constant    PORTA7_bit : integer := 7;
constant    PORTA6_bit : integer := 6;
constant    PORTA5_bit : integer := 5;
constant    PORTA4_bit : integer := 4;
constant    PORTA3_bit : integer := 3;
constant    PORTA2_bit : integer := 2;
constant    PORTA1_bit : integer := 1;
constant    PORTA0_bit : integer := 0;
 
-- Data Direction Register, Port A 
constant    DDA7_bit : integer := 7;
constant    DDA6_bit : integer := 6;
constant    DDA5_bit : integer := 5;
constant    DDA4_bit : integer := 4;
constant    DDA3_bit : integer := 3;
constant    DDA2_bit : integer := 2;
constant    DDA1_bit : integer := 1;
constant    DDA0_bit : integer := 0;
 
-- Input Pins, Port A 
constant    PINA7_bit : integer := 7;
constant    PINA6_bit : integer := 6;
constant    PINA5_bit : integer := 5;
constant    PINA4_bit : integer := 4;
constant    PINA3_bit : integer := 3;
constant    PINA2_bit : integer := 2;
constant    PINA1_bit : integer := 1;
constant    PINA0_bit : integer := 0;

-- Data Register, Port B 
constant    PB7_bit : integer := 7;
constant    PB6_bit : integer := 6;
constant    PB5_bit : integer := 5;
constant    PB4_bit : integer := 4;
constant    PB3_bit : integer := 3;
constant    PB2_bit : integer := 2;
constant    PB1_bit : integer := 1;
constant    PB0_bit : integer := 0;
 
-- Data Register, Port B 
constant    PORTB7_bit : integer := 7;
constant    PORTB6_bit : integer := 6;
constant    PORTB5_bit : integer := 5;
constant    PORTB4_bit : integer := 4;
constant    PORTB3_bit : integer := 3;
constant    PORTB2_bit : integer := 2;
constant    PORTB1_bit : integer := 1;
constant    PORTB0_bit : integer := 0;
 
-- Data Direction Register, Port B 
constant    DDB7_bit : integer := 7;
constant    DDB6_bit : integer := 6;
constant    DDB5_bit : integer := 5;
constant    DDB4_bit : integer := 4;
constant    DDB3_bit : integer := 3;
constant    DDB2_bit : integer := 2;
constant    DDB1_bit : integer := 1;
constant    DDB0_bit : integer := 0;
 
-- Input Pins, Port B 
constant    PINB7_bit : integer := 7;
constant    PINB6_bit : integer := 6;
constant    PINB5_bit : integer := 5;
constant    PINB4_bit : integer := 4;
constant    PINB3_bit : integer := 3;
constant    PINB2_bit : integer := 2;
constant    PINB1_bit : integer := 1;
constant    PINB0_bit : integer := 0;

-- Data Register, Port C 
constant    PC7_bit : integer := 7;
constant    PC6_bit : integer := 6;
constant    PC5_bit : integer := 5;
constant    PC4_bit : integer := 4;
constant    PC3_bit : integer := 3;
constant    PC2_bit : integer := 2;
constant    PC1_bit : integer := 1;
constant    PC0_bit : integer := 0;
 
-- Data Register, Port C 
constant    PORTC7_bit : integer := 7;
constant    PORTC6_bit : integer := 6;
constant    PORTC5_bit : integer := 5;
constant    PORTC4_bit : integer := 4;
constant    PORTC3_bit : integer := 3;
constant    PORTC2_bit : integer := 2;
constant    PORTC1_bit : integer := 1;
constant    PORTC0_bit : integer := 0;
 
-- Data Direction Register, Port C 
constant    DDC7_bit : integer := 7;
constant    DDC6_bit : integer := 6;
constant    DDC5_bit : integer := 5;
constant    DDC4_bit : integer := 4;
constant    DDC3_bit : integer := 3;
constant    DDC2_bit : integer := 2;
constant    DDC1_bit : integer := 1;
constant    DDC0_bit : integer := 0;
 
-- Input Pins, Port C 
constant    PINC7_bit : integer := 7;
constant    PINC6_bit : integer := 6;
constant    PINC5_bit : integer := 5;
constant    PINC4_bit : integer := 4;
constant    PINC3_bit : integer := 3;
constant    PINC2_bit : integer := 2;
constant    PINC1_bit : integer := 1;
constant    PINC0_bit : integer := 0;

-- Data Register, Port D 
constant    PD7_bit : integer := 7;
constant    PD6_bit : integer := 6;
constant    PD5_bit : integer := 5;
constant    PD4_bit : integer := 4;
constant    PD3_bit : integer := 3;
constant    PD2_bit : integer := 2;
constant    PD1_bit : integer := 1;
constant    PD0_bit : integer := 0;
 
-- Data Register, Port D 
constant    PORTD7_bit : integer := 7;
constant    PORTD6_bit : integer := 6;
constant    PORTD5_bit : integer := 5;
constant    PORTD4_bit : integer := 4;
constant    PORTD3_bit : integer := 3;
constant    PORTD2_bit : integer := 2;
constant    PORTD1_bit : integer := 1;
constant    PORTD0_bit : integer := 0;
 
-- Data Direction Register, Port D 
constant    DDD7_bit : integer := 7;
constant    DDD6_bit : integer := 6;
constant    DDD5_bit : integer := 5;
constant    DDD4_bit : integer := 4;
constant    DDD3_bit : integer := 3;
constant    DDD2_bit : integer := 2;
constant    DDD1_bit : integer := 1;
constant    DDD0_bit : integer := 0;
 
-- Input Pins, Port D 
constant    PIND7_bit : integer := 7;
constant    PIND6_bit : integer := 6;
constant    PIND5_bit : integer := 5;
constant    PIND4_bit : integer := 4;
constant    PIND3_bit : integer := 3;
constant    PIND2_bit : integer := 2;
constant    PIND1_bit : integer := 1;
constant    PIND0_bit : integer := 0;

-- SPI Status Register 
constant    SPIF_bit  : integer := 7;
constant    WCOL_bit  : integer := 6;
constant    SPI2X_bit : integer := 0;

-- SPI Control Register 
constant    SPIE_bit : integer := 7;
constant    SPE_bit  : integer := 6;
constant    DORD_bit : integer := 5;
constant    MSTR_bit : integer := 4;
constant    CPOL_bit : integer := 3;
constant    CPHA_bit : integer := 2;
constant    SPR1_bit : integer := 1;
constant    SPR0_bit : integer := 0;

-- Analog Comparator Control and Status Register 
constant    ACD_bit   : integer := 7;
constant    ACBG_bit  : integer := 6;
constant    ACO_bit   : integer := 5;
constant    ACI_bit   : integer := 4;
constant    ACIE_bit  : integer := 3;
constant    ACIC_bit  : integer := 2;
constant    ACIS1_bit : integer := 1;
constant    ACIS0_bit : integer := 0;

-- ADC Multiplexer Selection Register 
constant    REFS1_bit : integer := 7;
constant    REFS0_bit : integer := 6;
constant    ADLAR_bit : integer := 5;
constant    MUX4_bit  : integer := 4;
constant    MUX3_bit  : integer := 3;
constant    MUX2_bit  : integer := 2;
constant    MUX1_bit  : integer := 1;
constant    MUX0_bit  : integer := 0;

-- ADC Control and Status Register 
constant    ADEN_bit  : integer := 7;
constant    ADSC_bit  : integer := 6;
constant    ADFR_bit  : integer := 5;
constant    ADIF_bit  : integer := 4;
constant    ADIE_bit  : integer := 3;
constant    ADPS2_bit : integer := 2;
constant    ADPS1_bit : integer := 1;
constant    ADPS0_bit : integer := 0;

-- Data Register, Port E 
constant    PE7_bit : integer := 7;
constant    PE6_bit : integer := 6;
constant    PE5_bit : integer := 5;
constant    PE4_bit : integer := 4;
constant    PE3_bit : integer := 3;
constant    PE2_bit : integer := 2;
constant    PE1_bit : integer := 1;
constant    PE0_bit : integer := 0;
 
-- Data Register, Port E 
constant    PORTE7_bit : integer := 7;
constant    PORTE6_bit : integer := 6;
constant    PORTE5_bit : integer := 5;
constant    PORTE4_bit : integer := 4;
constant    PORTE3_bit : integer := 3;
constant    PORTE2_bit : integer := 2;
constant    PORTE1_bit : integer := 1;
constant    PORTE0_bit : integer := 0;
 
-- Data Direction Register, Port E 
constant    DDE7_bit : integer := 7;
constant    DDE6_bit : integer := 6;
constant    DDE5_bit : integer := 5;
constant    DDE4_bit : integer := 4;
constant    DDE3_bit : integer := 3;
constant    DDE2_bit : integer := 2;
constant    DDE1_bit : integer := 1;
constant    DDE0_bit : integer := 0;
 
-- Input Pins, Port E 
constant    PINE7_bit : integer := 7;
constant    PINE6_bit : integer := 6;
constant    PINE5_bit : integer := 5;
constant    PINE4_bit : integer := 4;
constant    PINE3_bit : integer := 3;
constant    PINE2_bit : integer := 2;
constant    PINE1_bit : integer := 1;
constant    PINE0_bit : integer := 0;	

end bit_def_pack;