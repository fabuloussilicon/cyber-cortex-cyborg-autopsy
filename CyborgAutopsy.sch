<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Ext_Trigger_Out" />
        <signal name="clk" />
        <signal name="XLXN_2" />
        <signal name="Serial_In" />
        <signal name="Serial_Out" />
        <signal name="Ext_Trigger_In" />
        <signal name="Ext_Clock_In" />
        <signal name="TX_LED" />
        <signal name="RX_LED" />
        <signal name="Data_In(31:0)" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="Ext_Clock_Out" />
        <signal name="Armed_LED" />
        <signal name="Trigger_LED" />
        <signal name="Reset" />
        <signal name="XLXN_38" />
        <signal name="XLXN_45" />
        <signal name="XLXN_46" />
        <signal name="XLXN_47" />
        <port polarity="Output" name="Ext_Trigger_Out" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="Serial_In" />
        <port polarity="Output" name="Serial_Out" />
        <port polarity="Input" name="Ext_Trigger_In" />
        <port polarity="Input" name="Ext_Clock_In" />
        <port polarity="Output" name="TX_LED" />
        <port polarity="Output" name="RX_LED" />
        <port polarity="BiDirectional" name="Data_In(31:0)" />
        <port polarity="Output" name="Ext_Clock_Out" />
        <port polarity="Output" name="Armed_LED" />
        <port polarity="Output" name="Trigger_LED" />
        <port polarity="Input" name="Reset" />
        <blockdef name="Clock32To50">
            <timestamp>2012-1-8T12:53:4</timestamp>
            <line x2="464" y1="32" y2="32" x1="400" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="464" y1="-96" y2="-96" x1="400" />
            <line x2="464" y1="-32" y2="-32" x1="400" />
            <rect width="336" x="64" y="-256" height="320" />
        </blockdef>
        <blockdef name="top_avr_core_v8">
            <timestamp>2012-1-13T12:30:3</timestamp>
            <line x2="0" y1="224" y2="224" x1="64" />
            <line x2="384" y1="224" y2="224" x1="320" />
            <line x2="0" y1="160" y2="160" x1="64" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="384" y1="32" y2="32" x1="320" />
            <line x2="384" y1="96" y2="96" x1="320" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <line x2="0" y1="-624" y2="-624" x1="64" />
            <line x2="0" y1="-512" y2="-512" x1="64" />
            <line x2="0" y1="-400" y2="-400" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-176" y2="-176" x1="64" />
            <line x2="384" y1="-736" y2="-736" x1="320" />
            <line x2="384" y1="-672" y2="-672" x1="320" />
            <line x2="384" y1="-608" y2="-608" x1="320" />
            <rect width="64" x="320" y="-364" height="24" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <rect width="64" x="320" y="-300" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-768" height="1024" />
        </blockdef>
        <blockdef name="SUMP_Core">
            <timestamp>2012-1-20T17:28:27</timestamp>
            <line x2="0" y1="288" y2="288" x1="64" />
            <line x2="0" y1="-464" y2="-464" x1="64" />
            <line x2="0" y1="-384" y2="-384" x1="64" />
            <line x2="0" y1="-304" y2="-304" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-144" y2="-144" x1="64" />
            <line x2="0" y1="-64" y2="-64" x1="64" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-576" height="896" />
        </blockdef>
        <block symbolname="Clock32To50" name="XLXI_2">
            <blockpin signalname="clk" name="CLKIN_IN" />
            <blockpin signalname="XLXN_47" name="CLKIN_IBUFG_OUT" />
            <blockpin name="CLK2X_OUT" />
            <blockpin signalname="XLXN_2" name="CLKFX_OUT" />
        </block>
        <block symbolname="top_avr_core_v8" name="XLXI_3">
            <blockpin signalname="Reset" name="nrst" />
            <blockpin signalname="XLXN_47" name="clk" />
            <blockpin signalname="XLXN_45" name="dataready" />
            <blockpin signalname="Serial_In" name="rxd" />
            <blockpin signalname="XLXN_46" name="spi_misoi" />
            <blockpin name="TMS" />
            <blockpin name="TCK" />
            <blockpin name="TDI" />
            <blockpin name="TRSTn" />
            <blockpin name="porta(7:0)" />
            <blockpin name="portb(7:0)" />
            <blockpin name="portc(7:0)" />
            <blockpin name="portd(7:0)" />
            <blockpin name="porte(7:0)" />
            <blockpin name="portf(7:0)" />
            <blockpin signalname="Serial_Out" name="txd" />
            <blockpin signalname="XLXN_25" name="spi_mosio" />
            <blockpin signalname="XLXN_26" name="spi_scko" />
            <blockpin signalname="XLXN_24" name="spi_cs_n" />
            <blockpin name="TDO" />
            <blockpin signalname="XLXN_38" name="reset_out" />
        </block>
        <block symbolname="SUMP_Core" name="XLXI_4">
            <blockpin signalname="XLXN_2" name="clockin" />
            <blockpin signalname="Ext_Clock_In" name="extClockIn" />
            <blockpin signalname="Ext_Trigger_In" name="extTriggerIn" />
            <blockpin signalname="XLXN_25" name="mosi" />
            <blockpin signalname="XLXN_26" name="sclk" />
            <blockpin signalname="XLXN_24" name="cs" />
            <blockpin signalname="XLXN_38" name="extReset" />
            <blockpin signalname="Data_In(31:0)" name="indata(31:0)" />
            <blockpin signalname="Ext_Clock_Out" name="extClockOut" />
            <blockpin signalname="Ext_Trigger_Out" name="extTriggerOut" />
            <blockpin signalname="XLXN_46" name="miso" />
            <blockpin signalname="XLXN_45" name="dataReady" />
            <blockpin signalname="Armed_LED" name="armLEDnn" />
            <blockpin signalname="Trigger_LED" name="triggerLEDnn" />
            <blockpin signalname="TX_LED" name="tx_test" />
            <blockpin signalname="RX_LED" name="rx_test" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="7040">
        <instance x="1584" y="1872" name="XLXI_2" orien="R0">
        </instance>
        <branch name="clk">
            <wire x2="1584" y1="1840" y2="1840" x1="1552" />
        </branch>
        <iomarker fontsize="28" x="1552" y="1840" name="clk" orien="R180" />
        <branch name="XLXN_2">
            <wire x2="2256" y1="1904" y2="1904" x1="2048" />
            <wire x2="2256" y1="1904" y2="2784" x1="2256" />
            <wire x2="2688" y1="2784" y2="2784" x1="2256" />
        </branch>
        <instance x="1312" y="3200" name="XLXI_3" orien="R0">
        </instance>
        <branch name="Serial_In">
            <wire x2="1312" y1="2576" y2="2576" x1="1280" />
        </branch>
        <iomarker fontsize="28" x="1280" y="2576" name="Serial_In" orien="R180" />
        <branch name="Serial_Out">
            <wire x2="1728" y1="2464" y2="2464" x1="1696" />
        </branch>
        <iomarker fontsize="28" x="1728" y="2464" name="Serial_Out" orien="R0" />
        <instance x="2688" y="2496" name="XLXI_4" orien="R0">
        </instance>
        <branch name="Ext_Trigger_In">
            <wire x2="2688" y1="2112" y2="2112" x1="2656" />
        </branch>
        <iomarker fontsize="28" x="2656" y="2112" name="Ext_Trigger_In" orien="R180" />
        <branch name="Ext_Clock_In">
            <wire x2="2688" y1="2032" y2="2032" x1="2656" />
        </branch>
        <iomarker fontsize="28" x="2656" y="2032" name="Ext_Clock_In" orien="R180" />
        <branch name="TX_LED">
            <wire x2="3104" y1="2336" y2="2336" x1="3072" />
        </branch>
        <iomarker fontsize="28" x="3104" y="2336" name="TX_LED" orien="R0" />
        <branch name="RX_LED">
            <wire x2="3104" y1="2400" y2="2400" x1="3072" />
        </branch>
        <iomarker fontsize="28" x="3104" y="2400" name="RX_LED" orien="R0" />
        <branch name="Data_In(31:0)">
            <wire x2="3104" y1="2464" y2="2464" x1="3072" />
        </branch>
        <iomarker fontsize="28" x="3104" y="2464" name="Data_In(31:0)" orien="R0" />
        <branch name="XLXN_24">
            <wire x2="2192" y1="2528" y2="2528" x1="1696" />
            <wire x2="2192" y1="2352" y2="2528" x1="2192" />
            <wire x2="2688" y1="2352" y2="2352" x1="2192" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="2176" y1="3232" y2="3232" x1="1696" />
            <wire x2="2176" y1="2192" y2="3232" x1="2176" />
            <wire x2="2688" y1="2192" y2="2192" x1="2176" />
        </branch>
        <branch name="XLXN_26">
            <wire x2="2160" y1="3296" y2="3296" x1="1696" />
            <wire x2="2160" y1="2272" y2="3296" x1="2160" />
            <wire x2="2688" y1="2272" y2="2272" x1="2160" />
        </branch>
        <branch name="Ext_Clock_Out">
            <wire x2="3104" y1="1952" y2="1952" x1="3072" />
        </branch>
        <branch name="Ext_Trigger_Out">
            <wire x2="3104" y1="2016" y2="2016" x1="3072" />
        </branch>
        <iomarker fontsize="28" x="3104" y="1952" name="Ext_Clock_Out" orien="R0" />
        <iomarker fontsize="28" x="3104" y="2016" name="Ext_Trigger_Out" orien="R0" />
        <branch name="Armed_LED">
            <wire x2="3360" y1="2208" y2="2208" x1="3072" />
        </branch>
        <iomarker fontsize="28" x="3360" y="2208" name="Armed_LED" orien="R0" />
        <branch name="Trigger_LED">
            <wire x2="3360" y1="2272" y2="2272" x1="3072" />
        </branch>
        <iomarker fontsize="28" x="3360" y="2272" name="Trigger_LED" orien="R0" />
        <branch name="Reset">
            <wire x2="1312" y1="3424" y2="3424" x1="1280" />
        </branch>
        <iomarker fontsize="28" x="1280" y="3424" name="Reset" orien="R180" />
        <branch name="XLXN_38">
            <wire x2="2208" y1="3424" y2="3424" x1="1696" />
            <wire x2="2208" y1="2432" y2="3424" x1="2208" />
            <wire x2="2688" y1="2432" y2="2432" x1="2208" />
        </branch>
        <branch name="XLXN_45">
            <wire x2="896" y1="3360" y2="3520" x1="896" />
            <wire x2="3760" y1="3520" y2="3520" x1="896" />
            <wire x2="1312" y1="3360" y2="3360" x1="896" />
            <wire x2="3760" y1="2144" y2="2144" x1="3072" />
            <wire x2="3760" y1="2144" y2="3520" x1="3760" />
        </branch>
        <branch name="XLXN_46">
            <wire x2="1312" y1="3232" y2="3232" x1="1088" />
            <wire x2="1088" y1="3232" y2="3472" x1="1088" />
            <wire x2="3344" y1="3472" y2="3472" x1="1088" />
            <wire x2="3344" y1="2080" y2="2080" x1="3072" />
            <wire x2="3344" y1="2080" y2="3472" x1="3344" />
        </branch>
        <branch name="XLXN_47">
            <wire x2="1232" y1="2352" y2="2464" x1="1232" />
            <wire x2="1312" y1="2464" y2="2464" x1="1232" />
            <wire x2="2128" y1="2352" y2="2352" x1="1232" />
            <wire x2="2128" y1="1776" y2="1776" x1="2048" />
            <wire x2="2128" y1="1776" y2="2352" x1="2128" />
        </branch>
    </sheet>
</drawing>